# ECE470 Project -- Beer-Pong Robot

## Usage

Click on ```ece470_sim.ttt``` after installing V-REP. 

```python
python3 beer_pong_robo_exec.py
```

## Historty
### [03-22-2020] Update 2
1. Add forward kinematic functions for the robot to calculate end factor's position given *theta*;
2. Vision sensor can detect the position of the spheres and do the frame transformation;
3. Set up the environment for Beer-Pong.
### [02-16-2020] Update 1 
1. Demonstrate using V-REP to control the robot simulator;
2. Accessing the data from the perspective vision sensor.

## Video Link
Update 2: [Youtube](https://youtu.be/tguRd_hfZQ0)
Update 1: [Youtube](https://youtu.be/QzZHsiDmg10)
## Contributors

Xinlong Sun(xs15)
Haozhe Si(haozhes3)