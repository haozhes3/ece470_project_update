# ECE470 Project Update 1

This is a demo project for demonstrating using V-REP to control the robot 
simulator and accessing the data from the perspective vision sensor.

## Usage

Click on ``ece470_sim.ttt`` after installing V-REP. 

```python
python3 checkpoint1.py
```

## Video Link

[Youtube](https://youtu.be/QzZHsiDmg10)

## Contributors

Xinlong Sun(xs15)

Haozhe Si(haozhes3)